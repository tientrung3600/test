define(['jquery','Magento_Ui/js/modal/confirm', 'uiComponent', 'ko','mage/url','mage/storage','Magento_Customer/js/customer-data'],
    function ($,confirmation, Component, ko ,urlBuilder, storage,customerData) {
    'use strict';
    function resultView(){
        var self = this;
        self.textButton = ko.observable('Choose from '+$('input[name="qty_color"]').val()+' colors'),
        self.viewOptions = function()
        {
            var self = this;
            var url = urlBuilder.build('snaply/index/getSwatch');
            var color = $('input[name="color"]').val();
            storage.post(
                url,
                JSON.stringify(color),
                false
            ).done(
                function (respone) {
                    console.log(respone);
                    confirmation({
                        title: '',
                        content: respone,
                        buttons: [{
                            text: $.mage.__('Add to cart'),
                            class: '',
                            click: function () {
                                this.closeModal();
                            }
                        }]
                    });
                }

            ).fail(
            );

        }
    };
    return Component.extend(new resultView());
});
