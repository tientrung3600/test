<?php

namespace AHT\Snaply\Controller\Index;

class GetSwatch extends \Magento\Framework\App\Action\Action
{
    protected $swatchHelper;

    /**
     * @param \Magento\Framework\Controller\Result\JsonFactory
     */
    private $_jsonFactory;

    /**
     * @param \Magento\Framework\Serialize\Serializer\Json
     */
    private $_json;

    protected $_storeManager;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Swatches\Helper\Data $swatchHelper
    ) {
        $this->swatchHelper = $swatchHelper;
        $this->_json = $json;
        $this->_jsonFactory = $jsonFactory;
        $this->_storeManager = $storeManager;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getContent();
        $response = $this->_json->unserialize($data);
        $colors = json_decode($response);
        $result = "";
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        foreach ($colors as $color) {
            $hashcodeData = $this->swatchHelper->getSwatchesByOptionsId([$color->color]);
            $c = $hashcodeData[$color->color]['value'];
            if ($hashcodeData[$color->color]['type']==1) {
                $result.= '<div class="option-view">';
                $result.= '<input class="swatch-options-view color" type="number" style="background: ' . $c . ' "/>';
                $result.= '<div class="swatch-text">' . $color->text . '</div>';
                $result.= '</div>';
            } else {
                $image = $baseUrl . 'media/attribute/swatch/swatch_image/30x20' . $c;
                $result.= '<div class="option-view" style="display: inline-block">';
                $result.= '<input class="swatch-options-view image" type="number" style="background: url(' . $image . ') "/>';
                $result.= '<div class="swatch-text">' . $color->text . '</div>';
                $result.= '</div>';
            }
        }
        $resultJson = $this->_jsonFactory->create();
        return $resultJson->setData($result);
    }
}
